import time
import datetime


def date_time_str():
    now = datetime.now()
    # dd/mm/YY H:M:S
    return now.strftime("%d.%m.%Y %H:%M:%S")


def ms_to_str(_duration):
    unit_str = 'ms'
    if _duration >= 0.1:
        unit_str = 'sec.'
        if _duration >= 60.0:
            unit_str = 'min'
            _duration /= 60.0
    return f'{_duration:.1f} {unit_str}'


class Timer:

    def __init__(self):
        self.start_time = None
        self.total_duration = 0

    @staticmethod
    def dur_str(_dur, _tag=None):
        time_str = ms_to_str(_dur)
        return time_str if _tag is None else f'{time_str} ({_tag})'

    def start(self):
        self.start_time = time.time()

    def interrupt(self):
        dur = time.time() - self.start_time
        self.total_duration += dur
        return dur

    def restart(self, _tag=None):
        dur = self.interrupt()
        self.start()
        print(Timer.dur_str(dur))

    def stop(self, _tag=None):
        self.interrupt()
        tot_dur = self.total_duration
        self.total_duration = 0
        return Timer.dur_str(tot_dur)

    def __str__(self):
        return f'Total duration: {ms_to_str(self.total_duration)}'
