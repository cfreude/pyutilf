import numpy as np
import scipy.misc


def map_to_uint8(img):
    imi = np.min(img)
    ima = np.max(img)
    r = ima - imi
    img = ((img - imi) / r) * 255
    return img.astype(np.uint8)


def linear_to_srgb(v):
    if v < 0.00313066844250063:
        return v * 12.92
    else:
        return 1.055 * np.power(v, 1.0/2.4) - 0.055


def rgb_to_srgb(img):
    srgb = np.vectorize(linear_to_srgb)
    return srgb(img)


def save_png(dest_path, src_img):
    scipy.misc.imsave(dest_path, src_img, mode=None)


def gamma(x, l=1.0/2.2):
    return np.power(x, l)


def luminance(rgb_img):
    dim = rgb_img.shape
    lf = [0.27, 0.67, 0.06] # [0.2126, 0.7152, 0.0722]
    to_lum = np.tile(lf, [dim[0], dim[1], 1])
    return np.sum(to_lum * rgb_img, axis=2, dtype=np.float64)


def tone_map(hdr_image, method='reinhard', apply_gamma=True, norm_func=None, mult_fac=None):

    hdr_image_lum = luminance(hdr_image)
    delta = 0.001

    tmp = np.log(delta + hdr_image_lum)
    l_average = np.exp(np.mean(tmp))
    l_white = np.mean(hdr_image_lum[hdr_image_lum > np.quantile(hdr_image_lum, 0.999)])

    key_a = np.max([0.0, 1.5 - 1.5 / (l_average * 0.1 + 1.0)]) + 0.1

    scaled = (key_a / l_average) * hdr_image

    ldr_image = np.zeros(hdr_image.shape, dtype=np.float64)

    method_dict = {
        'log': 0,
        'linear': 1,
        'reinhard': 2,
        'reinhard_mod': 3,
        'naive': 4,
        'lum': 5
    }

    if method_dict[method] == 0:  # log
        ldr_image = np.log(hdr_image + 0.001)

    elif method_dict[method] == 1:  # linear
        maxLum = np.max(hdr_image_lum)
        ldr_image = hdr_image / maxLum

    elif method_dict[method] == 2:  # reinhard
        ldr_image = scaled / (1.0 + scaled)

    elif method_dict[method] == 3:  # reinhard_mod
        ldr_image = (scaled * (1.0 + scaled / np.power(l_white, 2.0))) / (1.0 + scaled)

    elif method_dict[method] == 4:  # naive
        ldr_image = hdr_image / (hdr_image + 1.0)

    elif method_dict[method] == 5:  # lum
        #ldrImage = misc.normalize(luminance(hdrImage))
        ldr_image = luminance(hdr_image)

    elif method_dict[method] == 6:  # none
        ldr_image = hdr_image

    if norm_func is not None:
        ldr_image /= norm_func(ldr_image)

    if mult_fac is not None:
        ldr_image *= mult_fac

    if apply_gamma:
        ldr_image = gamma(ldr_image)

    return ldr_image


if __name__ == "__main__":
    pass
