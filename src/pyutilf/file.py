import os
import io
import json
import pickle
import errno


def check_postprefix(value, prefix=None, postfix=None):
    """
    Checks a string for matching prefix and postfix.

    If specified (not None) prefix and postfix must match for the function to return True,
    otherwise False is returned.

    Parameters
    ----------
    value : str
        string to check
    prefix : str
        matching prefix
    postfix : str
        matching postfix

    Returns
    -------
    bool
        True if both (if not None) match, otherwise False
    """

    prefix_match = True
    if prefix is not None and not value.startswith(prefix):
        prefix_match = False

    postfix_match = True
    if postfix is not None and not value.endswith(postfix):
        postfix_match = False

    return prefix_match and postfix_match


def get_file_paths(path, recursive=True, prefix=None, postfix=None):

    """
    Returns all paths to files matching prefix and/or postfix in path, or additionally recursively in all sub-folders.
    
    ...

    Parameters
    ----------
    path : str
        root path
    recursive : bool
        recursive sub-folder search flag
    prefix : str
        matching prefix
    postfix: str
        matching postfix
        
    Returns
    -------
    list
        List of matching file paths
    """

    file_list = []

    if recursive:
        for root, dirs, files in os.walk(path):

            # ignore files in root
            if root == path:
                continue

            for cur_file in files:
                if check_postprefix(cur_file, prefix, postfix):
                    file_list.append(os.path.join(root, cur_file))
    else:
        for cur_file in os.listdir(path):
            if check_postprefix(cur_file, prefix, postfix):
                file_list.append(os.path.join(path, cur_file))

    return file_list


def filter_file_paths(paths, included_substrings):
    filtered_paths = []
    for p in paths:
        include = False
        for s in included_substrings:
            include = include or s in p
        if include or len(included_substrings) < 1:
            filtered_paths.append(p)
    return filtered_paths


def create_folder(dir):
    try:
        os.makedirs(dir)
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise exc
        pass


def remove_files(path, postfix):
    paths = get_file_paths(path, recursive=False, prefix=None, postfix=postfix)
    for p in paths:
        os.remove(p)


def write_to_file(path, strings):
    with open(path, "w") as f:
        for s in strings:
            f.write(s)


def write_dict_to_json(path, dict):
    with io.open(path, 'w', encoding='utf-8') as f:
        d = json.dumps(dict, ensure_ascii=False)
        f.write(d)


def read_dict_from_json(path):
    with io.open(path, 'r', encoding='utf-8') as f:
        d = json.load(f)
    return d


def save_obj(obj, path):
    with open(path, 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


def load_obj(path):
    with open(path, 'rb') as f:
        return pickle.load(f)