import os
import math
import psutil
import platform


def byte_size_to_str(_size_bytes, _base=1024):
    # from https://stackoverflow.com/questions/5194057/better-way-to-convert-file-sizes-in-python
    if _size_bytes == 0:
        return '0B'
    size_name = ('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB')
    i = int(math.floor(math.log(_size_bytes, _base)))
    p = math.pow(_base, i)
    s = round(_size_bytes / p, 2)
    return f'{s} {size_name[i]}'


def print_memory_info():
    virtual_mem = psutil.virtual_memory()
    print('### Virtual Mem:')
    for k, v in [(k, getattr(virtual_mem, k, None)) for k in virtual_mem._fields]:
        print(k, byte_size_to_str(v))
    print('###')


def get_usable_memory(_buffer=4*(1024**3), _print=False):
    available_memory = psutil.virtual_memory().available
    usable_mem = available_memory - _buffer
    if _print:
        print('### Virtual Mem:')
        print('Available:', byte_size_to_str(available_memory))
        print('Buffer:', byte_size_to_str(_buffer))
        print('Usable Total:', byte_size_to_str(usable_mem))
        print('###')
    return usable_mem


def get_bytes_used_by_process():
    process = psutil.Process(os.getpid())
    return process.memory_info().vms


if platform.system() == 'Linux':

    import resource

    def get_memory_limit():
        return resource.getrlimit(resource.RLIMIT_AS)

    def set_memory_limit(maxsize):
        # clear swap: sudo swapoff -a && swapon -a
        # sudo sysctl vm.swappiness=10
        soft, hard = resource.getrlimit(resource.RLIMIT_AS)
        resource.setrlimit(resource.RLIMIT_AS, (maxsize, hard))

    def set_process_memory_limit_auto(_buffer=4*(1024**3)):
        maxsize = get_usable_memory(_buffer)
        print(f'Set process memory limit to {byte_size_to_str(maxsize)} (excl. {byte_size_to_str(_buffer)} buffer)')
        soft, hard = resource.getrlimit(resource.RLIMIT_AS)
        resource.setrlimit(resource.RLIMIT_AS, (maxsize, hard))
