import sys
import logging
import coloredlogs


# Create a logger object.
logger = logging.getLogger()

coloredlogs.install(
    level='DEBUG',
    stream=sys.stdout,
    fmt='%(asctime)s %(levelname)s %(message)s (%(name)s[%(process)d])',
    datefmt='%Y-%m-%d %H:%M:%S',
    level_styles={'info': {}, 'notice': {'color': 'magenta'}, 'verbose': {'color': 'blue'}, 'spam': {'color': 'green'},
                  'critical': {'color': 'red', 'bold': True}, 'error': {'color': 'red'}, 'debug': {'color': 'blue'},
                  'warning': {'color': 'yellow'}},
    field_styles={'hostname': {'color': 'black'}, 'programname': {'color': 'black'}, 'name': {'color': 'black'},
                  'levelname': {'color': 'black', 'bold': True}, 'asctime': {'color': 'black'}},
)

logger.setLevel('DEBUG')


def set_log_path(_name, _path, _lvl='INFO'):

    l = logging.getLogger(_name)

    for lh in logger.handlers:
        logger.removeHandler(lh)

    format = '%(asctime)s - %(levelname)s - %(message)s' if _lvl is 'INFO'\
        else '%(asctime)s - %(levelname)s - %(message)s - (%(name)s)'
    formatter = logging.Formatter(fmt=format, datefmt='%d/%m/%Y %I:%M:%S')

    console = logging.StreamHandler(sys.stdout)
    console.setLevel(_lvl)
    console.setFormatter(formatter)
    logger.addHandler(console)

    log_file = logging.FileHandler(_path, mode='w')
    log_file.setLevel(_lvl)
    log_file.setFormatter(formatter)
    logger.addHandler(log_file)

    return l


logger_lvl_override = None


def get_logger(name, lvl='INFO'):
    l = logging.getLogger(name)
    l.setLevel(lvl if logger_lvl_override is None else logger_lvl_override)
    return l


if __name__ == "__main__":
    # Some examples.
    logger.debug("this is a debugging message")
    logger.info("this is an informational message")
    logger.warn("this is a warning message")
    logger.error("this is an error message")
    logger.critical("this is a critical message")