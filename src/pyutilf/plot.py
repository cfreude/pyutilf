import os
import matplotlib.pyplop as plt
import matplotlib.ticker


def remove_ticks(_ax):
    _ax.set_yticklabels([])
    _ax.set_xticklabels([])
    _ax.set_xticks([])
    _ax.set_yticks([])


def save_figure(fig, root_path, name, dpi=500, size=(16, 10), close=False, tight=False):
    figure_path = os.path.join(root_path, name)
    print(f'Saving figure: {figure_path}')

    if tight:
        plt.gca().set_axis_off()
        plt.subplots_adjust(top=1, bottom=0, right=1, left=0,
                            hspace=0, wspace=0)
        plt.margins(0, 0)
        plt.gca().xaxis.set_major_locator(matplotlib.ticker.NullLocator())
        plt.gca().yaxis.set_major_locator(matplotlib.ticker.NullLocator())
        fig.savefig(figure_path, dpi=dpi, bbox_inches='tight', pad_inches=0)
    else:
        fig.set_size_inches(size, forward=False)
        fig.savefig(figure_path, dpi=dpi, bbox_inches='tight', pad_inches=0)

    if close:
        plt.close(fig)